
public class BasicBST {
   
   private BSTNode root;
   
   public BasicBST() {
      
      root = null;
   
   }

   private class BSTNode {
      
      public int element;
      public BSTNode left;
      public BSTNode right;   
      
   }
   
   public void insert(int data) {
      
      root = insert(data,root);
      
   }
   
   private BSTNode insert(int data, BSTNode treeroot) {
      
      if(treeroot == null) {
         treeroot = new BSTNode();
         treeroot.element = data;
      }
      else {
         if(data < treeroot.element)
            treeroot.left = insert(data,treeroot.left);
         else
            treeroot.right = insert(data,treeroot.right);
      }
      
      return treeroot;
   }
   
   public int countOdds() {
      
      return countOdds(root);
      
   }
   
   private int countOdds(BSTNode root) {
            
      if(root == null)
         return 0;
      else {
         
         if(root.element % 2 == 1)
            return countOdds(root.left) + countOdds(root.right) + 1;
         else
            return countOdds(root.left) + countOdds(root.right);
             
      }
      
   }
   
   public int height() {
      
      return max(root);
      
   }
   
   private int max(BSTNode root) {
      
      if(root == null)
         return -1;
      else {
         if(max(root.left) >= max(root.right))
            return max(root.left) + 1;
         else
            return max(root.right) + 1;
      }
   }
   
   public int countLeaves() {
      
      return countLeaves(root);
      
   }
   
   private int countLeaves(BSTNode root) {
      
      if(root == null)
         return 0;
      else {
         
         if(root.left == null && root.right == null)
         {
            return 1;
         }
         
         return countLeaves(root.left) + countLeaves(root.right);
         
      }
         
   }
   
   public int countOneChildParents() {
      
      return countOneChildParents(root);
      
   }
   
   private int countOneChildParents(BSTNode root) {
      
      if(root == null)
         return 0;
      else {
         if((root.left != null && root.right == null) || (root.left == null && root.right != null))
            return countOneChildParents(root.left) + countOneChildParents(root.right) + 1;  
         else
            return countOneChildParents(root.left) + countOneChildParents(root.right);
      }
      
   }
}
