import java.util.*;

public class BSTWork {

   public static void main(String[] args) {
    
      BasicBST bst = new BasicBST();
      Scanner input = new Scanner(System.in);
      char choice = 'z';
      int temp;
      
      System.out.println("Choose one of the following operations:");
      System.out.println("- add/insert (enter the letter a)");
      System.out.println("- count odds (enter the letter o)");
      System.out.println("- height (enter the letter h)");
      System.out.println("- count leaves (enter the letter l)");
      System.out.println("- count one child nodes (enter the letter c)");
      System.out.println("- quit (enter the letter q)");
      
      while(choice != 'q') {
         
         System.out.println("Enter choice: ");
         choice = input.nextLine().charAt(0);
         
         switch(choice) {
         
            case 'a':
               System.out.print("Enter number to be inserted: ");
               temp = input.nextInt();
               input.nextLine();
               bst.insert(temp);
               break;
            case 'o':
               System.out.println("Number of odds: " + bst.countOdds());
               break;
            case 'h':
               System.out.println("Height of tree: " + bst.height());
               break;
            case 'l':
               System.out.println("Number of leaves: " + bst.countLeaves());
               break;
            case 'c':
               System.out.println("Number of one child nodes: " + bst.countOneChildParents());
               break;
            case 'q':
               System.out.println("Quitting");
               break;
            default:
               System.out.println("Invalid Choice!");
               break;
         }
      }
      
   }
}
